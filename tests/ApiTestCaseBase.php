<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 2017-05-23
 * Time: 21:41
 */

namespace tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
//use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class ApiTestCaseBase extends WebTestCase {

    protected static $staticClient;

    public static function setUpBeforeClass() {
        self::$staticClient = static::createClient(['environment' => 'test']);
        self::bootKernel();
    }

    protected function setUp() {
        $this->client = self::$staticClient;
        $this->purgeDatabase();
    }


    protected function getService($serviceName){
        return self::$kernel->getContainer()->get($serviceName);
    }



    protected function createUser($userName, $password, $email) {
        $userManager = $this->getService('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setPlainPassword($password);
        $user->setUsername($userName);
        $user->setEmail($email);
        $userManager->updateUser($user);

        return $user;
    }

    private function purgeDatabase(){
        $purger = new ORMPurger($this->getService('doctrine')->getManager());
        $purger->purge();
    }

}