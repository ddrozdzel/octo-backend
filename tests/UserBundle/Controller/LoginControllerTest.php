<?php

namespace UserBundle\Tests\Controller;

use Tests\ApiTestCaseBase;

class LoginControllerTest extends ApiTestCaseBase
{

//    public function testAddNewUser() {
//        $data = [
//            'login' => 'daniel.drozdzel@gmail.com',
//            'password' => 'test123'
//        ];
//
//        $client = static::createClient();
//        $client->request(
//            'POST', '/users/login', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($data)
//        );
//
//        $this->assertEquals(201, $client->getResponse()->getStatusCode());
//        $this->assertEquals('token', $client->getResponse()->getContent());
//    }





    public function testLoginGoodUser() {
        $userName = 'daniel';
        $password = "daniel123";
        $email = 'daniel.drozdzel@gmail.com';

        $user = $this->createUser($userName, $password, $email);

        $this->client->request(
            'POST',
            '/users/login',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'PHP_AUTH_USER' => $userName,
                'PHP_AUTH_PW'   => $password,
            ]
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $responseArr = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('token', $responseArr);
    }

    public function testLoginWrongUser() {
        $userName = 'daniel';
        $password = "daniel123";
        $email = 'daniel.drozdzel@gmail.com';

        $user = $this->createUser($userName, $password, $email);

        $this->client->request(
            'POST',
            '/login',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'PHP_AUTH_USER' => $userName,
                'PHP_AUTH_PW'   => 'wrong_password',
            ]
        );

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $responseArr = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('Not Found', $responseArr['error']['message']);
    }


}


