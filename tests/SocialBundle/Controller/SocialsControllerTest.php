<?php

namespace SocialBundle\Tests\Controller;

use SocialBundle\Controller\SocialsController;
use Lakion\ApiTestCase\JsonApiTestCase;

//TODO: Testing With Database Fixtures

class SocialstControllerTest extends JsonApiTestCase
{
    public function testGetSocials() {
        $this->loadFixturesFromDirectory();

        $this->client->request(
            'GET', '/app/socials', array(), array(), array('CONTENT_TYPE' => 'application/json')
        );

        $response = $this->client->getResponse();
        $this->assertResponse($response, 'social_get');
    }

    public function testAddNewSocialAction() {
        $this->client->request(
            'POST', '/app/socials', array(), array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"userID":"123456","expires":"3600","social": "facebook","token":"token1"}'
        );

        $response = $this->client->getResponse();
        $this->assertResponse($response, 'add_social');
    }


    public function testUpdateSocialAction() {
        $this->client->request(
            'PUT', '/app/socials/2', array(), array(), array('CONTENT_TYPE' => 'application/json'),
            '{"userID":"123456","expires":"3600","social": "facebook","token":"token2"}'
        );

        $response = $this->client->getResponse();
        $this->assertResponse($response, 'update_social');
    }


    public function testDeleteSocialAction() {
        $this->client->request(
            'DELETE', '/app/socials/2', array(), array(), array('CONTENT_TYPE' => 'application/json')
        );

        $response = $this->client->getResponse();
        $this->assertResponse($response, 'delete_social');
    }

}
