<?php

namespace OauthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OauthBundle:Default:index.html.twig');
    }
}
