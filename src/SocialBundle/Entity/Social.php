<?php

namespace SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Social
 *
 * @ORM\Table(name="social")
 * @ORM\Entity(repositoryClass="SocialBundle\Repository\SocialRepository")
 */
class Social
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @Assert\NotBlank()
     */
    private $uid;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     * @Assert\Choice(choices = { "facebook", "twitter", "linkedin" }, message = "Choose a valid social media.")
     */
    private $social;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank()
     * @Assert\Length(max = 200, maxMessage = "Your first name cannot be longer than {{ limit }} characters")
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank()
     * @Assert\Length(min = 6, max= 200,
     *     minMessage = "Your first name must be at least {{ limit }} characters long",
     *     maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=1023)
     * @Assert\NotBlank()
     */
    private $userPic;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\Type(type="bool", message="The value {{ value }} is not a valid {{ type }}.")
     */
    private $active;

    /**
     * @ORM\Column(type="datetime",)
     * @Assert\NotBlank()
     * @Assert\Type(type="datetime", message="The value {{ value }} is not a valid {{ type }}.")
     *
     */
    private $expireDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $registrationDate;

    /**
     * @ORM\Column(type="integer",  nullable=true)
     */
    private $updateTime;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param mixed $social
     */
    public function setSocial($social)
    {
        $this->social = $social;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getUserPicUrl()
    {
        return $this->userPic;
    }

    /**
     * @param mixed $userPic
     */
    public function setUserPicUr($userPic)
    {
        $this->userPic = $userPic;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * @param mixed $token
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
    }


    public function setRegistrationDate() {
        $this->registrationDate =  time();
    }

    public function setUpdateTime() {
        $this->updateTime = time();
    }



}

