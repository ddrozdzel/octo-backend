<?php

namespace SocialBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EnityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use SocialBundle\Entity\Social;

/**
* @Security("is_granted('ROLE_USER')")
*/
class SocialsController extends Controller {
    private $em;

    public function getSocialsAction(Request $request) {
        $this->setMenager();
        $socials = $this->getSocialsQuery($this->getUidByBerear($request));
        return new JsonResponse($socials);
    }

    public function addNewSocialAction(Request $request) {
        $this->setMenager();
        $em = $this->em->getManager();
        $content = $this->getContent($request);
        $this->validateRequestValues($content, ['userID', 'token','social', 'expires']);

        $oauthData = $this->getFromOauth($content);

        $newSocialAccount = new Social();
        $newSocialAccount->setUid($this->getUser()->getId());
        $newSocialAccount->setLogin($oauthData['login']);
        $newSocialAccount->setToken($oauthData['token']);
        $newSocialAccount->setSocial($oauthData['social']);
        $newSocialAccount->setExpireDate($oauthData['expireDate']);
        $newSocialAccount->setUserPicUr($oauthData['userPicUrl']);
        $newSocialAccount->setActive(true);
        $newSocialAccount->setRegistrationDate();

        $this->validateEntity($newSocialAccount);
        $em->persist($newSocialAccount);
        $em->flush();
        return new JsonResponse(["social"=>$oauthData['social'], 'login'=>$oauthData['login']]);
    }

    public function updateSocialAction($id,Request $request) {
        $this->setMenager();
        $em = $this->em->getManager();

        $socialAccount = $this->checkIfSocialAccountExists($id);
        $content = $this->getContent($request);

        $this->validateRequestValues($content, ['userID', 'token','social', 'expires']);
        $oauthData = $this->getFromOauth($content);

        foreach($oauthData as $key=>$data) {
        	$key = ucfirst($key);
            if($socialAccount->{'get'.$key}() != $data) {
				$socialAccount->{'set'.$key}($data);
            }
        }
        $socialAccount->setUpdateTime();
        $this->validateEntity($socialAccount);
        $em->flush();
        return new JsonResponse(["status"=>"updated"]);

    }

    public function deleteSocialAction($id) {
        $this->setMenager();
        $em = $this->get('doctrine')->getManager();

        $socialAccount = $this->checkIfSocialAccountExists($id);
        $em->remove($socialAccount);
        $em->flush();
        return new JsonResponse(["status"=>"deleted"]);
    }

    private function setMenager() {
        $this->em = $this->getDoctrine();
    }

    private function getSocialsQuery($uid) {
        $fields = ['soc.id', 'soc.social', 'soc.userPic', 'soc.login', 'soc.active'];
        $query = $this->em->getRepository('SocialBundle:Social')->createQueryBuilder('soc')->select($fields)->where('soc.uid = :uid')->setParameter('uid', $uid)->getQuery();
        return $query->getResult();
    }

    private function checkIfSocialAccountExists($id) {
        if (!$socialAccount = $this->em->getRepository('SocialBundle:Social')->find($id)) {
            throw new \Exception('No social accunt found for id '.$id);
        }
        return $socialAccount;
    }

    private function getContent(Request $request) {
      if(!$data = json_decode($request->getContent(), true)) {
          throw new Exception('Wrong json data format');
      }
      return $data;
    }

    private function validateEntity($entity) {
        $validator = $this->get('validator');
        $errors = $validator->validate($entity);
        $this->checkForErrors($errors);
    }

    private function validateRequestValues($requestArray, $needValues) {
       foreach($needValues as $value) {
           if(!isset($requestArray[$value])) {
               throw new Exception("There is no $value key in request");
           }

       }
    }

    private function checkForErrors($errors) {
        if (count($errors) > 0) {
            throw new Exception((string) $errors);
        }
    }

    private function getUidByBerear(Request $request) {
    	return 2;
		if(!empty($token = $this->extractTokenFromBearer($request))){
			return !empty($id = $this->matchToRedis($token)) ? $id : $this->getUidFromMysql($token);
		}
		throw new \Exception('Token does not match user id');
    }

    private function extractTokenFromBearer(Request $request) {
		$extractor = new AuthorizationHeaderTokenExtractor('Bearer', 'Authorization');
		if(!$token = $extractor->extract($request)) {
			throw new \Exception('Token does not match user id');
		}

		return $token;
	}

	private function pushTokenToRedis($token) {
		$redis = $this->container->get('snc_redis.default');
		$redis->set($this->getUser()->getId(), $token);
	}




    private function getFromOauth(Array $content) {
		$fbSDK = $this->container->get('facebook_sdk');

		$tokenInfo = $fbSDK->getLongLivedToken($content['token']);
		$userInfo = $fbSDK->getFromFbGraph(['picture{url}','name'], $tokenInfo['tokenValue']);

		$this->pushTokenToRedis($tokenInfo['tokenValue']);


		return [
			'login'		 => $userInfo['name'],
			'token' 	 => $tokenInfo['tokenValue'],
			'social'	 => $content['social'],
			'expireDate' => $tokenInfo['expires'],
			'userPicUrl' => $userInfo['picture']['url']
		];

    }

}
/*
 * curl -X GET http://127.0.0.1:8000/app/socials -H "Authorization: Bearer eyJhbGciOiJSUzI1NiJ9.eyJ1c2VybmFtZSI6ImRhbmllbCIsImV4cCI6IjE0OTY2OTc2ODgiLCJpYXQiOjE0OTY2OTQwODh9.MD5QKXwQDBokHFSTYr6Prwl9aNUj0BFQn4H2NOkCYjz1fWw6muRtNpKYuaP36Mg44YeUzqTHeQcD5rwmmRJyyABI5RudpKMz-r82Kzr0u6-d3S8oY2kicb-ghB5fn32arfmLNvmJ3U-HCVm1sPLdw0op7kSXYE-hAdOSHkjaLlXBtMMSilIYK-Wos9m2NVA8z1OCD6EYn-AeULk_NPeivr7fcq9r1_Ha532-Jn4gOGvHoAUM8QjtdkgwLED9ecZEMqlRWnt3hwRDHNC399Mjkm6nRXIqFaV0u905vOzx4e1YdZ5NMf45l-ewGcQUbApgByoMazp470rp5AH4F4hap2RmFjSZO2_Uk4_xOltpL1jNWIrOk0ePA78gCbKXiIhF73c8WKoW1E7py7REvHymC-wTznirae041xa1hhueerseEZkBP6IrFNT9QTJWE_C6UR4YgX-_A6hsSfiGcT_w07eE4yGggrx5-w11LdevJcBKEtz7sYQNWn6LeL4VlsDLrOEoNy5kk8sIeERFjRa3qZtC5_-xeWxW3wiyGS7ArHMAgJVywNQnzegXSrnsFXfCXpK_uH84LiESv2eXuofVEL65UqxOoMUDQNJrnSF-4Pg7K3gwOd04n4UkixbkGFMtaYHMX7Cxcely4sF2TH55fzctdu-owl77fQPlo1Uq1QM"


curl -d '{"userID":"123456","expires":"3600","social": "facebook","token":"asdasdasdasdsdsdsd"}' -H "Content-Type: application/json" -X POST http://127.0.0.1:8000/app/socials


curl -X DELETE http://127.0.0.1:8000/app/socials/1

curl -d '{"id": "1", "login":"daniel.drozdzel","password":"test12345"}' -H "Content-Type: application/json" -X PUT http://127.0.0.1:8000/app/socials/1
 */
