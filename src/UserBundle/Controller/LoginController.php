<?php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\User;

class LoginController extends Controller {

    /**
     * @Route("/login", name="user_login")
     * @Method("POST")
     */

    public function loginAction(Request $request) {
       // $userName = $request->getUser();
       // $password = $request->getPassword();

		$content = $this->getContent($request);
		$this->validateRequestValues($content, ['login', 'password']);

        $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(['username' => $content['login']]);

        if (!$user)
            throw $this->createNotFoundException();



        $isValid = $this->get('security.password_encoder')->isPasswordValid($user, $content['password']);
        //if (!$isValid)
        //    throw new BadCredentialsException();

        $token = $this->getToken($user);

		//file_put_contents('/home/daniel/log.txt', print_r(new JsonResponse(['token' => $token]), true));

        return new JsonResponse(['token' => $token]);
    }


    /*
     * Get jwt_aut tokens by user
     *
     * @param User $user
     * @return array
     */

    public function getToken(User $user)
    {
        return $this->container->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => $this->getTokenExpiryDateTime(),
            ]);
    }

    /**
     * Returns token expiration datetime.
     *
     * @return string Unixtmestamp
     */
    private function getTokenExpiryDateTime()
    {
        $tokenTtl = $this->container->getParameter('lexik_jwt_authentication.token_ttl');
        $now = new \DateTime();
        $now->add(new \DateInterval('PT'.$tokenTtl.'S'));

        return $now->format('U');
    }


	private function getContent(Request $request) {
		if(!$data = json_decode($request->getContent(), true)) {
			throw new Exception('Wrong json data format');
		}
		return $data;
	}

	private function validateRequestValues($requestArray, $needValues) {
		foreach($needValues as $value) {
			if(!isset($requestArray[$value])) {
				throw new Exception("There is no $value key in request");
			}

		}
	}



}


//http://matkodjipalo.com/index.php/2016/08/10/symfony-rest-api-user-login/
//http://stackoverflow.com/questions/35120309/symfony-2-fosuserbundle-with-rest-login-and-registration