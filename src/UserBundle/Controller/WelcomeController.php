<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 2017-05-28
 * Time: 22:53
 */


namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Security("is_granted('ROLE_USER')")
 */
class WelcomeController extends Controller
{
    /**
     * @Route("/welcome", name="user_welcome")
     * @Method("GET")
     */
    public function welcomeAction(Request $request)
    {
        $response = new Response(json_encode('Hello user.'), Response::HTTP_OK);

        return $response;
    }
}

/*
 * curl -X POST http://127.0.0.1:8000/users/login -F "username=daniel" -F "password=daniel123"
 *
 * curl -X GET http://127.0.0.1:8000/users/welcome -H "Authorization: Bearer token
 *
 */