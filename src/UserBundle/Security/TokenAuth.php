<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 2017-05-28
 * Time: 23:00
 */

namespace UserBundle\Security;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuth extends AbstractGuardAuthenticator{

    private $jwtEncoder;
    private $em;

    public function __construct(JWTEncoderInterface $jwtEncoder, EntityManager $em)
    {
        $this->jwtEncoder = $jwtEncoder;
        $this->em = $em;
    }

    public function getCredentials(Request $request){
        $extractor = new AuthorizationHeaderTokenExtractor('Bearer', 'Authorization');
        if(!$token = $extractor->extract($request))
            return null;
        return $token;
    }

    public function getUser($credentials, UserProviderInterface $userProvider){
        $data = $this->jwtEncoder->decode($credentials);
        if(!$data && !isset($data['username'])) {
            throw new CustomUserMessageAuthenticationException('Invalid Token');
        }
        return $this->em->getRepository('UserBundle:User')->findOneBy(['username'=>$data['username']]);

    }

    public function checkCredentials($credentials, UserInterface $user){
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
    }

    public function supportsRememberMe(){
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null){
        return new Response('Token is missing!', Response::HTTP_UNAUTHORIZED);
    }

}