<?php

namespace FacebookBundle\Services;

use Facebook;
use Facebook\Authentication\AccessToken;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 12.07.2017
 * Time: 11:07
 */
class FacebookSDKService {
	private $app_id;
	private $app_secret;
	private $default_graph_version;
	private $fbSDK;
	private $OAuth2Client;

	public function __construct($app_id, $app_secret,$default_graph_version ) {
	    var_dump("czas ". time());
		if (empty($app_id) && empty($app_secret) && empty($default_graph_version))
			throw new \Exception(__CLASS__."constructor have wrong params:".
			"app_id - ".$app_id.PHP_EOL.
			"app_secret - ".$app_secret.PHP_EOL.
			"default_graph_version - ".$default_graph_version.PHP_EOL
			);

		$this->app_id = (string) $app_id;
		$this->app_secret = $app_secret;
		$this->default_graph_version = $default_graph_version;
		$this->createFacebookSDKObject();
	}

	public function getLongLivedToken($accessToken) {
		$this->setOAuth2Client();
		$tokenMetadata = $this->OAuth2Client->debugToken($accessToken);
		$tokenObjEntity = new AccessToken($accessToken);
		if($tokenObjEntity->isLongLived()) {
			return $accessToken;
		}
		$this->validateToken($tokenMetadata);
		$longLivedToken = $this->OAuth2Client->getLongLivedAccessToken($accessToken);
		return ['tokenValue' => $longLivedToken->getValue(), 'expires'=>$longLivedToken->getExpiresAt()];
	}


	private function createFacebookSDKObject(){

		$this->fbSDK = new Facebook\Facebook([
			'app_id' => $this->app_id,
			'app_secret' => $this->app_secret,
			'default_graph_version' => $this->default_graph_version
		]);

		$this->setOAuth2Client();
	}

	private function setOAuth2Client() {
		if(empty($this->OAuth2Client))
			$this->OAuth2Client = $this->fbSDK->getOAuth2Client();
	}

	private function validateToken($tokenMetadata) {
		$tokenMetadata->validateAppId($this->app_id);
		$tokenMetadata->validateExpiration();
	}

	public function getFromFbGraph($fields ,$accessToken) {
		$fetchData = $this->fbSDK->get($this->generateGetQuery($fields), $accessToken);
		return $fetchData->getGraphUser();
	}

	private function generateGetQuery($fields, $context = 'me') {
		return '/'.$context.'?fields='.implode(",", $fields);
	}

}